open the terminal
open the directory these files are in
run command "python cherrypy_example.py"

once the server is running:

Open your browser and navigate to "localhost:10005/login" to login

navigate to "localhost:10005/msg" to send a message to another user.
the format for the "IP and port" field is: "100.10.10.10:10000"

find a user's IP and port by navigating to "localhost:10005/showusers"

navigate to "localhost:10005/logout" to sign off.
