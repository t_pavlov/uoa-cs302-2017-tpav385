#!/usr/bin/python
""" cherrypy_example.py

    COMPSYS302 - Software Design
    Author: Andrew Chen (andrew.chen@auckland.ac.nz)
    Last Edited: 19/02/2015

    This program uses the CherryPy web server (from www.cherrypy.org).
"""
# Requires:  CherryPy 3.2.2  (www.cherrypy.org)
#            Python  (We use 2.7)

# The address we listen for connections on
listen_ip = "0.0.0.0"
listen_port = 10005

import sqlite3
import cherrypy
import urllib
import urllib2
import json
import time
import hashlib

class MainApp(object):

    #CherryPy Configuration
    _cp_config = {'tools.encode.on': True, 
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on' : 'True',
                 }                 

    # If they try somewhere we don't know, catch it here and send them to the right place.
    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    # PAGES (which return HTML that can be viewed in browser)
    @cherrypy.expose
    def index(self):
        Page = "Welcome! This is a test website for COMPSYS302!<br/>"
        
        try:
            Page += "Hello " + cherrypy.session['username'] + "!<br/>"
            Page += "Here is some bonus text because you've logged in!"
        except KeyError: #There is no username
            
            Page += "Click here to <a href='login'>login</a>."
        return Page
        
    @cherrypy.expose
    def login(self):
        Page = '<form action="/logintoserver" method="post" enctype="multipart/form-data">'
        Page += 'Username: <input type="text" name="username"/><br/>'
        Page += 'Password: <input type="text" name="password"/>'
        Page += '<input type="submit" value="Login"/></form>'
        return Page
        
    @cherrypy.expose
    def msg(self):
        Page = '<form action="/send_message" method="post" enctype="multipart/form-data">'
        Page += 'Recepient: <input type="text" name="UPI"/><br/>'
        Page += 'IP and Port: <input type="text" name="IPport"/><br/>'
        Page += 'Message: <input type="text" name="message"/>'
        Page += '<input type="submit" value="Send Message"/></form>'
        return Page
    
    @cherrypy.expose    
    def sum(self, a=0, b=0): #All inputs are strings by default
        output = int(a)+int(b)
        return str(output)
        
    # LOGGING IN AND OUT
    @cherrypy.expose
    def signin(self, username=None, password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        error = self.authoriseUserLogin(username,password)
        if (error == 0):
            cherrypy.session['username'] = username;
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/login')

    @cherrypy.expose
    def signout(self):
        """Logs the current user out, expires their session"""
        username = cherrypy.session.get('username')
        if (username == None):
            pass
        else:
            cherrypy.lib.sessions.expire()
        raise cherrypy.HTTPRedirect('/')
        
    def authoriseUserLogin(self, username, password):
        print(username)
        print(password)
        if (username.lower() == "andrew") and (password.lower() == "password"):
            return 0
        else:
            return 1

    @cherrypy.expose
    def logintoserver(self,username,password):
		salt = password +"COMPSYS302-2017"
		encrypt=hashlib.sha256(salt).hexdigest()
		loadserver = urllib2.urlopen('http://cs302.pythonanywhere.com/report?username=%s&password=%s&location=0&ip=10.103.137.41&port=10005'%(username,encrypt))
		readserver = loadserver.read()
		print(readserver)
		Page = "server data: " + str(readserver) + "<br/>"
		return Page





    @cherrypy.expose
    def logout(self):
        loadserver = urllib2.urlopen('http://cs302.pythonanywhere.com/logoff?username=tpav385&password=3db05c2c132a89c20603233b90a405f1292673ee4ad5c9307b1cdddf56ee5d5f')
        readserver = loadserver.read()
        print(readserver)
        Page = "server data: " + str(readserver) + "<br/>"
        return Page

    @cherrypy.expose
    def showusers(self):
        loadlist = urllib2.urlopen('http://cs302.pythonanywhere.com/getList?username=tpav385&password=3db05c2c132a89c20603233b90a405f1292673ee4ad5c9307b1cdddf56ee5d5f&json=1')
        readlist = loadlist.read().decode('utf-8')
        decodedread = json.loads(readlist)
        conn = sqlite3.connect('list_database.db')
        c = conn.cursor()
        c.execute('DROP TABLE userData')
        c.execute('CREATE TABLE IF NOT EXISTS userData(ip TEXT, location TEXT, lastLogin TEXT, username TEXT, port TEXT)')
        for i in range(len(decodedread)):
            c.execute('INSERT INTO userData(ip, location, lastLogin, username, port) VALUES(?, ?, ?, ?, ?)',
                      ((decodedread[str(i)]['ip']),(decodedread[str(i)]['location']),(decodedread[str(i)]['lastLogin']),(decodedread[str(i)]['username']),(decodedread[str(i)]['port'])))
            print(decodedread[str(i)])
            c.execute('SELECT username FROM userData')
            listofusers = c.fetchall()
            conn.commit()
        c.execute('SELECT username, ip, port FROM userData')
        listofusers = c.fetchall()
        Page = str(listofusers)
        c.close()
        conn.close()

        #Page = "LIST OF USERS:<br/>" + str(decodedread['1'])
        return Page


    @cherrypy.expose
    def send_message(self,UPI,IPport, message):
		
        output_dict = { "sender" : "tpav385",
                        "message" : message,
                        "destination" : UPI,
                        "stamp" : time.time()}
        jsondata = json.dumps(output_dict)
        url = 'http://' + IPport + '/receiveMessage'
        req = urllib2.Request(url, jsondata, {'Content-Type':'application/json'})
        response = urllib2.urlopen(req)


    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveMessage(self):
        input_dict = cherrypy.request.json
        print(input_dict)
        return '0'

def runMainApp():
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), "/")

    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                           })

    print("=========================")
    print("University of Auckland")
    print("COMPSYS302 - Software Design Application")
    print("========================================")
    
    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()
 
#Run the function to start everything
runMainApp()
